##Lets get things done

**Well hello there, you seem to have found yourself in a fun place**

This application is to demonstrate an extremely simple todo list, though this application is 100% local, I plan on making this a user based application so you can see the evolution of this project

*The following will show you how to run this application in any browser, we recommend the new [Firefox](http://mozilla.org/download)*

---

## Run the application

You’ll start by heading over to the **'index.html'** file .

1. Double clicking the 'index.html file' will generate the application in your default browser
2. Secondary clicking (right clicking) on the 'index.html' file will give you your systems opens for opening the file in any application you desire

---

## Functionality
**Adding Todo Items**
1. Select the box that states **'Add New Todo'**
2. Type in item todo
3. Press enter key on keyboard to add item

**Mark as Completed Todo Items**
1. Select the text of the item
2. Item will now have gray strike through signifying completion

**Delete Todo Items**
1. Hover over todo item
2. Delete button will appear on left side of item
3. Select trash can icon for todo item deletion

##Current Bugs
* Accepts empty string as todo item
    * Prompt signifying todo text is empty when pressing enter
* Todo items are not being pulled from database
    * Need to create backend infrastructure for user signin and todo database
* Just 1 todo list, user feedback suggest multiple todo list, (ex. Grocery, Work, Life)