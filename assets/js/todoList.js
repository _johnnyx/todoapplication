//Check off speciic Todos by clicking
$('ul').on('click','li', function () {
    $(this).toggleClass('completed')
});

//Click on X to delete
$('ul').on('click','span', function (event) {
    $(this).parent().fadeOut(750, function () {
        $(this).remove()
    });

    event.stopPropagation()
})

$('input[type="text"]').keypress(function (event) {
    if(event.which === 13)
    {
        var todo = document.createElement('li');
        todo.innerText = $(this).val()
        $('<span><i class="fa fa-trash-o"></i> </span>').prependTo(todo)
        $(this).val('')
        $('ul').append(todo)
    }
})

$('.fa-plus-square-o').click(function () {
    $('input[type="text"]').fadeToggle()
})